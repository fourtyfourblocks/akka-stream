/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.ffb.demo.akka.streams.producer;

import akka.stream.io.Framing;
import akka.stream.javadsl.FlattenStrategy;
import akka.stream.javadsl.Flow;
import akka.stream.javadsl.Source;
import akka.util.ByteString;
import akka.util.ByteStringBuilder;
import com.ffb.demo.akka.streams.producer.di.ProducerModule;
import com.ffb.demo.akka.streams.producer.service.Producer;
import com.ffb.demo.akka.streams.producer.tcp.TcpEventServer;
import com.fourtyfourblocks.akka.di.BaseModule;
import com.fourtyfourblocks.akka.di.guice.GuiceDiModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.runtime.BoxedUnit;

import java.io.IOException;
import java.nio.ByteOrder;

/**
 * @since : 2015-07-07
 */
public class ProducerServiceApp
{
    private final static Logger logger = LoggerFactory.getLogger(ProducerServiceApp.class);

    public static void main(String[] args) throws IOException
    {
        Injector injector = Guice.createInjector(new BaseModule(), new GuiceDiModule(), new ProducerModule());
        logger.info("Initialized modules.");

        final int port = Integer.parseInt(args[0]);
        logger.info("Starting producer at {}.", port);

        final Producer producer = injector.getInstance(Producer.class);

        final Flow<Long, ByteString, BoxedUnit> encoder = Flow
                .of(Long.class)
                .map(number -> {
                    logger.trace("on next number {}.", number);
                    final ByteStringBuilder builder = new ByteStringBuilder();
                    builder.putByte((byte) 8);
                    builder.putLong(number, ByteOrder.BIG_ENDIAN);

                    return builder.result();
                })
                .named("encoder");

        final ByteString delimiter = ByteString.fromInts(Integer.MIN_VALUE, Integer.MAX_VALUE);
        final Source<ByteString, BoxedUnit> binaryNumbers = producer
                .produce(1000, 10)
                .via(encoder);

        final int maximumFrameLength = 512;
        final Flow<ByteString, ByteString, BoxedUnit> handler = Flow.of(ByteString.class)
                .via(Framing.delimiter(delimiter, maximumFrameLength, false))
                .map(ByteString::utf8String)
                .map(in -> {
                    logger.info("Incoming {}.", in);
                    return binaryNumbers;
                })
                .flatten(FlattenStrategy.<ByteString, BoxedUnit>concat());

        injector
                .getInstance(TcpEventServer.class)
                .bindTcpAs("localhost", port, handler);

//        final Source<Integer, ActorRef> source = Source.actorRef(10, OverflowStrategy.backpressure());
//        final ActorRef ref = source
//                .map(i -> i + 1)
//                .to(Sink.foreach(i -> logger.info("next " + i)))
//                .run(materializer);
//
//        final Props publisherProps = Props.create(() -> new MessageProducer(4));
//        final Source<String, ActorRef> stringSource = Source.actorPublisher(publisherProps);
//        final ActorRef ref2 = stringSource
//                .map(String::toLowerCase)
//                .to(Sink.foreach(i -> logger.info("next " + i)))
//                .run(materializer);
//
//        ref.tell(1, ActorRef.noSender());
//        ref2.tell("Send this out", ActorRef.noSender());
//
//        logger.info("Started producer.", port);
//        logger.info("<ENTER> to exit!", port);
//        System.in.read();
//
//        logger.info("Shutting down producer.");
//        akkaSystem.shutdown();
    }
}
    
