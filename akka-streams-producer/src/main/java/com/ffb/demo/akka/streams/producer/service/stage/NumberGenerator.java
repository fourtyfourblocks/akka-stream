/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.ffb.demo.akka.streams.producer.service.stage;

import akka.stream.stage.Context;
import akka.stream.stage.PushPullStage;
import akka.stream.stage.SyncDirective;
import akka.util.ByteString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author : pawel
 *         description :
 * @since : 2015-08-10
 */
public class NumberGenerator extends PushPullStage<Integer, ByteString>
{
    private final static Logger logger = LoggerFactory.getLogger(NumberGenerator.class);

    @Override
    public SyncDirective onPush(Integer elem, Context<ByteString> ctx)
    {
        logger.info("on push {}.", elem);
        return ctx.push(ByteString.fromInts(elem));
    }

    @Override
    public SyncDirective onPull(Context<ByteString> ctx)
    {
        logger.info("on pull.");
        return ctx.pull();
    }
}
    
