/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.ffb.demo.akka.streams.producer;

import akka.actor.ActorSystem;
import com.ffb.demo.akka.streams.producer.di.ProducerModule;
import com.fourtyfourblocks.akka.di.BaseModule;
import com.fourtyfourblocks.akka.di.guice.GuiceDiModule;
import com.fourtyfourblocks.akka.message.Message;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import java.util.UUID;

/**
 * @author : pawel
 * @since : 2015-07-07
 */
public class ProducerApp
{
    private final static Logger logger = LoggerFactory.getLogger(ProducerApp.class);

    public static void main(String[] args) throws Exception
    {
        Injector injector = Guice.createInjector(new BaseModule(), new GuiceDiModule(), new ProducerModule());
        logger.info("Initialized modules.");

        MDC.put(Message.MDC_LOGGER_KEY, UUID.randomUUID().toString());
        logger.info("Starting producer.");

        LocalStreamProcessor processor = injector.getInstance(LocalStreamProcessor.class);
        processor.process();

        logger.info("Shutting down producer.");
        Named named = Names.named(BaseModule.PRODUCER_AKKA_SYSTEM);
        injector.getInstance(Key.get(ActorSystem.class, named)).shutdown();
    }
}
    
