/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.ffb.demo.akka.streams.producer.di;

import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.stream.ActorMaterializer;
import akka.stream.javadsl.Flow;
import akka.util.ByteString;
import akka.util.ByteStringBuilder;
import com.ffb.demo.akka.streams.producer.LocalStreamProcessor;
import com.ffb.demo.akka.streams.producer.actor.LocalMessageLogger;
import com.ffb.demo.akka.streams.producer.actor.MessageProducer;
import com.ffb.demo.akka.streams.producer.http.EventDispatcher;
import com.ffb.demo.akka.streams.producer.service.Producer;
import com.ffb.demo.akka.streams.producer.tcp.TcpEventServer;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.name.Named;
import scala.runtime.BoxedUnit;

import javax.inject.Singleton;
import java.nio.ByteOrder;
import java.util.concurrent.TimeUnit;

/**
 * @author : pawel
 *         file name   : AppModule
 *         <p>
 *         description :
 * @since : 2015-07-07
 */
public class ProducerModule extends AbstractModule
{
    @Override
    protected void configure()
    {
        bind(Producer.class);
        bind(EventDispatcher.class);
        bind(LocalStreamProcessor.class);
        bind(TcpEventServer.class);
    }

    @Provides
    @Singleton
    public ActorMaterializer streamMaterializer(ActorSystem producerSystem)
    {
        return ActorMaterializer.create(producerSystem);
    }

    @Provides
    @Singleton
    @Named("localMessageLogger")
    public Props localMessageLoggerProps()
    {
        return Props.create(LocalMessageLogger.class, () -> new LocalMessageLogger(5, TimeUnit.SECONDS));
    }

    @Provides
    @Singleton
    @Named("messageProducer")
    public Props messageProducerProps()
    {
        return Props.create(MessageProducer.class, () -> new MessageProducer(5));
    }

    @Provides
    @Singleton
    @Named("integerToByteStringEncoder")
    public Flow<Long, ByteString, BoxedUnit> integerToByteStringEncoder()
    {
        return Flow
                .of(Long.class)
                .map(number -> {
                    final ByteStringBuilder builder = new ByteStringBuilder();
                    builder.putByte((byte) 4);
                    builder.putLong(number, ByteOrder.BIG_ENDIAN);

                    return builder.result();
                })
                .named("encoder");
    }
}
    
