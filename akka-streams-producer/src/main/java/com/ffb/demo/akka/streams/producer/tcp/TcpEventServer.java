/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.ffb.demo.akka.streams.producer.tcp;

import akka.actor.ActorSystem;
import akka.http.javadsl.Http;
import akka.http.javadsl.IncomingConnection;
import akka.http.javadsl.ServerBinding;
import akka.stream.ActorMaterializer;
import akka.stream.javadsl.Flow;
import akka.stream.javadsl.Source;
import akka.stream.javadsl.Tcp;
import akka.util.ByteString;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.concurrent.Future;

import static akka.stream.javadsl.Sink.foreach;

/**
 * @author : pawel
 *         file name   : TcpEventServer
 *         <p>
 *         description :
 * @since : 2015-07-22
 */
public class TcpEventServer
{
    private final static Logger logger = LoggerFactory.getLogger(TcpEventServer.class);
    private final ActorSystem system;
    private final ActorMaterializer materializer;

    @Inject
    TcpEventServer(ActorSystem system,
                   ActorMaterializer materializer)
    {
        this.system = system;
        this.materializer = materializer;
    }

    public Source<IncomingConnection, Future<ServerBinding>> initializeHttp(String host, int port)
    {
        return Http
                .get(system)
                .bind(host, port, materializer);

//        final Future<ServerBinding> bindFuture = bind
//                .to(foreach(con -> {
//                    logger.info("Accepted connection from {}.", con.remoteAddress());
////                    con.handleWith()
//                }))
//                .run(materializer);
    }

    public <T> Future<Tcp.ServerBinding> bindTcpAs(String host, int port,
                                                   Flow<ByteString, ByteString, T> handlerFlow)
    {
        return Tcp
                .get(system)
                .bind(host, port)
                .to(foreach(con -> con.handleWith(handlerFlow, materializer)))
                .run(materializer);
    }

    void startEchoTcp(String host, int port)
    {
//        final Flow<Integer, ByteString, BoxedUnit> encoder = Flow
//                .of(Integer.class)
//                .map(number -> {
//                    final ByteStringBuilder builder = new ByteStringBuilder();
//                    builder.putByte((byte) 4);
//                    builder.putInt(number, ByteOrder.BIG_ENDIAN);
//
//                    return builder.result();
//                })
//                .named("encoder");
//        final akka.stream.scaladsl.Flow<ByteString, Integer, BoxedUnit> decoder = Framing
//                .lengthField(1, 0, 5, ByteOrder.BIG_ENDIAN)
//                .map(bytes -> {
//                    final ByteIterator iterator = bytes.iterator();
//                    iterator.next();
//                    return iterator.getInt(ByteOrder.BIG_ENDIAN);
//                })
//                .named("decoder");

    }
}
    
