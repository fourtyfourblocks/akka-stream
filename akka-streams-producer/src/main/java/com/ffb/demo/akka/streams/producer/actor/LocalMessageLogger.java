/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.ffb.demo.akka.streams.producer.actor;

import akka.actor.AbstractActor;
import akka.actor.ReceiveTimeout;
import com.fourtyfourblocks.akka.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.concurrent.duration.Duration;

import java.util.concurrent.TimeUnit;

import static akka.japi.pf.ReceiveBuilder.match;

/**
 * @author : pawel
 * @since : 2015-07-21
 */
public class LocalMessageLogger extends AbstractActor {
    private final static Logger logger = LoggerFactory.getLogger(LocalMessageLogger.class);

    private long count;
    private Message lastSeenMsg;

    public LocalMessageLogger(int logDelay, TimeUnit unit) {
        logger.info("Initializing LocalMessageConsumer.");
        receive(match(Message.class,
                message -> {
                    lastSeenMsg = message;
                    count += 1;
                })
                .match(ReceiveTimeout.class,
                        rt -> {
                            logger.info("Received {} messages so far\n.Last seen message {}.", count, lastSeenMsg);
                            getContext().setReceiveTimeout(Duration.create(logDelay, unit));
                        })
                .matchAny(this::unhandled)
                .build());
    }
}
    
