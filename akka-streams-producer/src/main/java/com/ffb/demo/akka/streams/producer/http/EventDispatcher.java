/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.ffb.demo.akka.streams.producer.http;

import akka.http.javadsl.model.ContentType;
import akka.http.javadsl.model.HttpEntities;
import akka.http.javadsl.model.HttpResponse;
import akka.http.javadsl.model.MediaTypes;
import akka.http.javadsl.model.StatusCodes;
import akka.http.javadsl.server.ExceptionHandler;
import akka.http.javadsl.server.HttpApp;
import akka.http.javadsl.server.RequestContext;
import akka.http.javadsl.server.Route;
import akka.http.javadsl.server.RouteResult;
import akka.stream.javadsl.Flow;
import akka.stream.javadsl.Source;
import akka.util.ByteString;
import com.ffb.demo.akka.streams.producer.http.model.HealthEvent;
import com.ffb.demo.akka.streams.producer.service.Producer;
import com.google.inject.name.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.runtime.BoxedUnit;

import javax.inject.Inject;
import java.util.UUID;

import static akka.http.javadsl.marshallers.jackson.Jackson.json;
import static akka.http.javadsl.model.HttpResponse.create;
import static akka.http.javadsl.model.MediaTypes.APPLICATION_OCTET_STREAM;

/**
 * @author : pawel
 *         file name   : EventDispatcher
 *         <p>
 *         description :
 * @since : 2015-07-07
 */
public class EventDispatcher extends HttpApp
{
    private final static Logger logger = LoggerFactory.getLogger(EventDispatcher.class);
    private final Producer producer;
    private final Flow<Long, ByteString, BoxedUnit> encoder;
    private final ExceptionHandler errorHandler = e -> {
        logger.error("Catched error", e);
        return complete(create().withStatus(StatusCodes.INTERNAL_SERVER_ERROR));
    };

    @Inject
    EventDispatcher(Producer producer,
                    @Named("integerToByteStringEncoder") Flow<Long, ByteString, BoxedUnit> encoder)
    {
        this.producer = producer;
        this.encoder = encoder;
    }

    @Override
    public Route createRoute()
    {
        final Route route = route(get(pathSingleSlash().route(complete(MediaTypes.TEXT_HTML.toContentType(), "ok"))),
                                  get(path("health").route(handleWith(this::health))),
                                  get(path("producer").route(handleWith(this::getEvents))),
                                  get(path("producer", "settings").route(handleWith(this::readConfiguration))),
                                  post(path("producer", "settings").route(handleWith(this::updateProducer))));
        return handleExceptions(errorHandler, route);
    }

    private RouteResult readConfiguration(RequestContext ctx)
    {
        return ctx.complete("ok.");
    }

    private RouteResult getEvents(RequestContext ctx)
    {

        final Source<Long, BoxedUnit> source = producer.produce(10, 100);
        final Source<ByteString, Object> data = source.via(encoder).mapMaterializedValue(m -> m);
        final HttpResponse response = HttpResponse.create()
                .withEntity(HttpEntities.createChunked(ContentType.create(APPLICATION_OCTET_STREAM), data));
        return ctx.complete(response);
    }

    private RouteResult updateProducer(RequestContext ctx)
    {
        return ctx.complete("update ok." + ctx.request().getUri().queryString());
    }

    private RouteResult health(RequestContext ctx)
    {
        return ctx.completeAs(json(), new HealthEvent("1.0", UUID.randomUUID().toString()));
    }
}
    
