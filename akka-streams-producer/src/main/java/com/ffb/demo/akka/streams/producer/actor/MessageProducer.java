/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.ffb.demo.akka.streams.producer.actor;

import akka.actor.PoisonPill;
import akka.actor.Status;
import akka.japi.pf.ReceiveBuilder;
import akka.stream.actor.AbstractActorPublisher;
import akka.stream.actor.ActorPublisherMessage;
import com.fourtyfourblocks.akka.message.Fail;
import com.fourtyfourblocks.akka.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.concurrent.duration.Duration;

import java.util.ArrayDeque;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import static java.lang.Long.min;

/**
 * passes accepted messages to materialized stream.
 * the producer will buffer limited number of messages and notify sender if buffer is overflown with {@link Fail} message.
 *
 * @since : 2015-07-21
 */
public class MessageProducer extends AbstractActorPublisher<Message>
{
    private final static Logger logger = LoggerFactory.getLogger(MessageProducer.class);
    private final ArrayDeque<Message> buf;

    public MessageProducer(int maxBufferSize)
    {
        buf = new ArrayDeque<>(maxBufferSize);
        receive(ReceiveBuilder
                        .match(Message.class, msg -> buf.size() == maxBufferSize,
                               msg -> {
                                   if (logger.isTraceEnabled())
                                   {
                                       logger.trace("Denying {}. buffer size is {}.", msg, buf.size());
                                   }
                                   sender().tell(new Fail<>(msg.getMDC(), "denied"), self());
                               })
                        .match(Message.class,
                               msg -> {
                                   buf.addLast(msg);
                                   drain(totalDemand());
                               })
                        .match(ActorPublisherMessage.Request.class, request -> drain(totalDemand()))
                        .match(ActorPublisherMessage.Cancel.class, cancel -> stop())
                        .match(ActorPublisherMessage.SubscriptionTimeoutExceeded.class, cancel -> stop())
                        .match(Status.Success.class, cancel -> stop())
                        .match(PoisonPill.class, cancel -> stop())
                        .matchAny(this::unhandled)
                        .build()
        );
    }

    @Override
    public Duration subscriptionTimeout()
    {
        return Duration.create(1, TimeUnit.SECONDS);
    }

    private void drain(long demand)
    {
        final int bufferSize = buf.size();
        logger.debug("Stream is active {}. {}", isActive(), bufferSize);
        if (!isActive())
        {
            return;
        }

        long maxItems = min(demand, bufferSize);
        logger.debug("Draining buffer with {} items. demand is {}.", maxItems, demand);
        Stream
                .iterate(0, i -> i + 1)
                .limit(maxItems)
                .forEach(i -> {
                    Message msg = buf.poll();
                    logger.trace("Sending message {}.", msg);
                    onNext(msg);
                });
    }

    private void stop()
    {
        context().stop(self());
    }
}
