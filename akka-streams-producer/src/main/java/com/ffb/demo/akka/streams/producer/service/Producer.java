/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.ffb.demo.akka.streams.producer.service;

import akka.stream.javadsl.Source;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.runtime.BoxedUnit;

/**
 * @author : pawel
 *         file name   : Producer
 *         <p>
 *         description :
 * @since : 2015-07-07
 */
public class Producer
{
    private final static Logger logger = LoggerFactory.getLogger(Producer.class);

    public Source<Long, BoxedUnit> produce(int delay, int limit)
    {
        return Source
                .from(new NumberProducer(delay, limit));
    }
}
    
