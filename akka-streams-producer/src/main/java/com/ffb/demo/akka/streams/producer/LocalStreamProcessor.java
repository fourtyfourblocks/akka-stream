package com.ffb.demo.akka.streams.producer;

import akka.actor.ActorRef;
import akka.actor.PoisonPill;
import akka.actor.Props;
import akka.stream.ActorMaterializer;
import akka.stream.OverflowStrategy;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import com.fourtyfourblocks.akka.message.Message;
import com.fourtyfourblocks.akka.message.Result;
import com.google.inject.name.Named;
import com.jayway.awaitility.Awaitility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import static akka.actor.ActorRef.noSender;

public class LocalStreamProcessor {

    private final static Logger logger = LoggerFactory.getLogger(LocalStreamProcessor.class);
    private final ActorMaterializer materializer;
    private final Props producerProps;

    @Inject
    LocalStreamProcessor(ActorMaterializer materializer,
                         @Named("messageProducer") Props producerProps)
    {
        this.materializer = materializer;
        this.producerProps = producerProps;
    }

    public void process() throws Exception
    {
        final List<Integer> numbers = new ArrayList<>(100);
        final List<String> messages = new ArrayList<>(100);
        final Source<Integer, ActorRef> source = Source.actorRef(10, OverflowStrategy.dropNew());


        final ActorRef numberRef = source
                .map(i -> i + 1)
                .to(Sink.foreach(numbers::add))
                .run(materializer);

        final Source<Message, ActorRef> stringSource = Source.actorPublisher(producerProps);
        final ActorRef producerRef = stringSource
                .map(msg -> msg.toString().toLowerCase())
                .to(Sink.foreach(messages::add))
                .run(materializer);

        final int requestedElementsCount = 100;
        Thread thread = new Thread(() -> {
            logger.info("Executing in thread.");
            Stream.iterate(0, i -> i + 1)
                    .limit(requestedElementsCount)
                    .forEach(i -> {
                        numberRef.tell(i, noSender());
                        producerRef.tell(new Result<>("Index " + i), noSender());

                        //                        sleepUninterruptibly(1, TimeUnit.MILLISECONDS);
                    });
        });
        thread.start();

        Awaitility
                .await("consume all")
                .atMost(20, TimeUnit.SECONDS)
                .until(() -> {
                    logger.info("Got so far {}, {}", numbers.size(), messages.size());
                    assert numbers.size() == requestedElementsCount;
                    assert messages.size() == requestedElementsCount;
                });

        logger.info("numbers {}.", numbers);
        logger.info("messages {}.", messages);
        logger.info("killing producer");
        producerRef.tell(PoisonPill.getInstance(), noSender());
    }
}
