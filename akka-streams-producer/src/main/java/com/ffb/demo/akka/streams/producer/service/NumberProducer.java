/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.ffb.demo.akka.streams.producer.service;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Stream;

import static com.google.common.util.concurrent.Uninterruptibles.sleepUninterruptibly;

/**
 * @since : 2015-07-21
 */
public class NumberProducer implements Publisher<Long>
{
    private final static Logger logger = LoggerFactory.getLogger(NumberProducer.class);

    private final long limit;
    private final int delay;

    public NumberProducer(int delay, long limit)
    {
        this.limit = limit;
        this.delay = delay;
    }

    @Override
    public void subscribe(Subscriber<? super Long> subscriber)
    {
        final AtomicLong tempLimit = new AtomicLong(limit);
        final AtomicLong tempIndex = new AtomicLong(0);
        subscriber.onSubscribe(new Subscription()
        {
            @Override
            public void request(long demand)
            {
                final long currentLimit = tempLimit.get();
                final long index = tempIndex.get();
                logger.trace("Producing events starting from {} to {}, demand is {}.", index, currentLimit, demand);
                Stream
                        .iterate(index, i -> i + 1)
                        .limit(Long.min(demand, currentLimit))
                        .forEach((t) -> {
                            sleepUninterruptibly(delay, TimeUnit.MILLISECONDS);
                            subscriber.onNext(t);
                        });
                long nextLimit = currentLimit - demand;
                if (nextLimit <= 0)
                {
                    nextLimit = 0;
                    subscriber.onComplete();
                }
                tempLimit.compareAndSet(currentLimit, nextLimit);
                tempIndex.compareAndSet(index, index + demand);
            }

            @Override
            public void cancel()
            {
                tempLimit.getAndSet(0);
                subscriber.onComplete();
            }
        });
    }
}
