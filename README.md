# overview
play with reactive streams, see differences with rxJava and akka

# documentation
## streams
- http://doc.akka.io/docs/akka-stream-and-http-experimental/1.0/java.html

## akka
- http://doc.akka.io/docs/akka/current/java.html