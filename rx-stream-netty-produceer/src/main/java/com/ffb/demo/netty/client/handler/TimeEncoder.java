/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.ffb.demo.netty.client.handler;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author : pawel
 *         file name   : TimeDecoder
 *         <p>
 *         description :
 * @since : 2015-07-18
 */
public class TimeEncoder extends MessageToByteEncoder
{

    private final static Logger logger = LoggerFactory.getLogger(TimeEncoder.class);

    @Override
    protected void encode(ChannelHandlerContext ctx, Object msg, ByteBuf out) throws Exception
    {
        logger.info("Encoding {}.", msg);
        final Integer index = (Integer) msg;
        out.writeInt(index);
    }
}
    
