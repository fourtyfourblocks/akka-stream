/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.ffb.demo.netty.client.handler;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.AttributeKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.util.concurrent.CompletableFuture;

/**
 * @author : pawel
 *         file name   : TimeClientHandler
 *         <p>
 *         description :
 * @since : 2015-07-18
 */
@ChannelHandler.Sharable
public class TimeClientHandler extends ChannelInboundHandlerAdapter
{
    private final static Logger logger = LoggerFactory.getLogger(TimeClientHandler.class);
    private final AttributeKey<CompletableFuture<Instant>> responseFutureKey = AttributeKey.valueOf("responseFuture");

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg)
    {
        Instant time = (Instant) msg;
        logger.info("Received time response {}.", time);

        ctx.attr(responseFutureKey).get().complete(time);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
    {
        logger.error("Error handling call for current time.", cause);
    }
}
    
