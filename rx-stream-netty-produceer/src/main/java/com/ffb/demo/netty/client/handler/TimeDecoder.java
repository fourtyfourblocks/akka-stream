/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.ffb.demo.netty.client.handler;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.util.List;

/**
 * @author : pawel
 *         file name   : TimeDecoder
 *         <p>
 *         description :
 * @since : 2015-07-18
 */
public class TimeDecoder extends ByteToMessageDecoder
{
    private final static Logger logger = LoggerFactory.getLogger(TimeDecoder.class);

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out)
    {
        if (in.readableBytes() < 4)
        {
            return;
        }

        logger.info("Converting long to instant.");
        final long epoch = in.readUnsignedInt();
        out.add(Instant.ofEpochMilli(epoch));
    }
}
    
