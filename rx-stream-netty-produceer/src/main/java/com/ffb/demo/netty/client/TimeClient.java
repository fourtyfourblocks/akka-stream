/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.ffb.demo.netty.client;

import com.ffb.demo.netty.client.handler.TimeClientHandler;
import com.ffb.demo.netty.client.handler.TimeDecoder;
import com.ffb.demo.netty.client.handler.TimeEncoder;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPromise;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author : pawel
 *         file name   : TimeClient
 *         <p>
 *         description :
 * @since : 2015-07-18
 */
public class TimeClient
{
    private final static Logger logger = LoggerFactory.getLogger(TimeClient.class);

    private final Channel channel;

    public TimeClient(int port)
    {
        channel = bootstrap(port);
    }

    private static void extractTime(Future<Instant> f)
    {
        logger.info("Reading time response.");
        final Instant result;
        try
        {
            result = f.get(1, TimeUnit.SECONDS);
            logger.info("Response is {}.", result);
        }
        catch (InterruptedException | ExecutionException | TimeoutException e)
        {
            logger.error("Could not read time", e);

        }
    }

    public static void main(String[] args)
    {
        int count = 1;
        int port = 8080;
        if (args.length > 1)
        {
            count = Integer.parseInt(args[0]);
            port = Integer.parseInt(args[1]);
        }
        else
        {
            logger.error("You have to provide parallel call count and port to bind to.");
            System.exit(-1);
        }
        logger.info("Starting {} time clients, calling time service at {}.", count, port);

//        final ExecutorService executors = Executors.newFixedThreadPool(count);

        final TimeClient client = new TimeClient(port);

        final List<Future<Instant>> results = Stream.iterate(0, (n) -> n + 1)
                .limit(count)
                .map(client::send)
                .collect(Collectors.toList());

        results.stream()
                .forEach(TimeClient::extractTime);
    }

    private Channel bootstrap(int port)
    {
        EventLoopGroup workerGroup = new NioEventLoopGroup();

        Bootstrap b = new Bootstrap();
        b.group(workerGroup);
        b.channel(NioSocketChannel.class);
        b.option(ChannelOption.SO_KEEPALIVE, true);
        b.handler(new ChannelInitializer<SocketChannel>()
        {
            @Override
            public void initChannel(SocketChannel ch) throws Exception
            {
                ch.pipeline()
                        .addLast(new TimeDecoder())
                        .addLast(new TimeEncoder())
                        .addLast(new TimeClientHandler());
            }
        });

        try
        {
            return b.connect("localhost", port).sync().channel();
        }
        catch (InterruptedException e)
        {
            logger.error("Could not initialize channel.", e);
            throw new IllegalStateException("Could not initialize channel", e);
        }
    }

    private Future<Instant> send(int index)
    {
        final ChannelPromise promise = channel.newPromise();
        channel.writeAndFlush(index, promise);

        final CompletableFuture<Instant> future = new CompletableFuture<>();
        future.complete(Instant.now());
        return future;
    }
}
    
