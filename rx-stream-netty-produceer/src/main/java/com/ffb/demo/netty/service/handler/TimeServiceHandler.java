/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */

package com.ffb.demo.netty.service.handler;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author : pawel
 *         file name   : TimeServiceHandler
 *         <p>
 *         description :
 * @since : 2015-07-18
 */
public class TimeServiceHandler extends ChannelInboundHandlerAdapter
{
    private final static Logger logger = LoggerFactory.getLogger(TimeServiceHandler.class);

    @Override
    public void channelActive(final ChannelHandlerContext ctx)
    {
        logger.info("timer invoked");
        final ByteBuf time = ctx.alloc().buffer(4);
        time.writeInt((int) (System.currentTimeMillis() / 1000L + 2208988800L));

        ctx.writeAndFlush(time);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
    {
        cause.printStackTrace();
        ctx.close();
    }
}
    
