/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.ffb.demo.akka.streams.consumer.actor;

import akka.actor.AbstractActor;
import akka.actor.ReceiveTimeout;
import akka.japi.pf.ReceiveBuilder;
import com.ffb.demo.akka.streams.consumer.model.Answer;
import com.ffb.demo.akka.streams.consumer.model.Config;
import com.ffb.demo.akka.streams.consumer.model.InProgress;
import com.ffb.demo.akka.streams.consumer.model.Produce;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.Option;
import scala.PartialFunction;
import scala.concurrent.duration.Duration;
import scala.runtime.BoxedUnit;

import javax.inject.Inject;
import java.util.concurrent.TimeUnit;
import java.util.function.BinaryOperator;

/**
 * @author : pawel
 *         description :
 * @since : 2015-09-17
 */
public class WorkerActor extends AbstractActor
{
    private final static Logger logger = LoggerFactory.getLogger(WorkerActor.class);

    private final PartialFunction<Object, BoxedUnit> productionBhv;
    private Config config;
    private Produce pendingTask;

    @Inject
    public WorkerActor()
    {
        productionBhv = ReceiveBuilder
                .match(Produce.class, this::decline)
                .match(ReceiveTimeout.class, r -> finish(pendingTask))
                .matchAny(this::unhandled)
                .build();
        receive(ReceiveBuilder
                        .match(Config.class,
                               config -> {
                                   this.config = config;
                               })
                        .match(Produce.class, this::startProducing)
                        .matchAny(this::unhandled)
                        .build()
        );
    }

    @Override
    public void preStart() throws Exception
    {
        if (config != null)
        {
            logger.info("{} : starting .. checking if there is pending task ..", config.getId());
            if (pendingTask != null)
            {
                self().tell(pendingTask, pendingTask.getRef());
            }
        }
    }

    @Override
    public void postRestart(Throwable reason) throws Exception
    {
        logger.error(config.getId() + " : after restarting with reason.", reason);
        super.postRestart(reason);
    }

    @Override
    public void preRestart(Throwable reason, Option<Object> message) throws Exception
    {
        logger.error(config.getId() + " : before restarting with reason.", reason);
        super.preRestart(reason, message);
    }

    @Override
    public void postStop() throws Exception
    {
        logger.info("{} : actor has just stoped.", config.getId());
    }

    private void startProducing(Produce task)
    {
        context().become(productionBhv);

        this.pendingTask = task;
        context().setReceiveTimeout(Duration.apply(config.getSeed() * 10, TimeUnit.MILLISECONDS));
        logger.info("{} : Suspending computation for {} millis.", config.getId(), config.getSeed() * 10);
    }

    private void finish(Produce task)
    {
        context().setReceiveTimeout(Duration.Undefined());
        logger.info("{} : Restarting computation.", config.getId());
        final BinaryOperator<Integer> addFn = (a, b) -> a + b;
        final Integer sum = task.getItems().stream()
                .reduce(0, addFn, addFn);

        final Answer answer = new Answer(config.getId(), task.getId(), sum);
        logger.info("{} : Sending {} back.", config.getId(), answer);

        task.getRef().tell(answer, self());
        this.pendingTask = null;
        context().unbecome();
    }

    private void decline(Produce msg)
    {
        logger.warn("Cannot handle another task before one finishes, {}.", msg);
        context().sender().tell(new InProgress(msg.getId()), self());
    }
}
    
