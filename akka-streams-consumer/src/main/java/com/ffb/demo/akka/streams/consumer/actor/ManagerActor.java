/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.ffb.demo.akka.streams.consumer.actor;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.ReceiveTimeout;
import akka.japi.pf.ReceiveBuilder;
import com.ffb.demo.akka.streams.consumer.model.Answer;
import com.ffb.demo.akka.streams.consumer.model.Config;
import com.ffb.demo.akka.streams.consumer.model.InProgress;
import com.ffb.demo.akka.streams.consumer.model.Produce;
import com.fourtyfourblocks.akka.di.AbstractActorProps;
import com.google.common.collect.Iterators;
import com.google.inject.name.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.concurrent.duration.Duration;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author : pawel
 *         description :
 * @since : 2015-09-17
 */
public class ManagerActor extends AbstractActor
{
    private final static Logger logger = LoggerFactory.getLogger(ManagerActor.class);

    private final int expandRate;
    private final int numberOfWorkers;
    private final Map<UUID, Produce> items = new HashMap<>();
    private final Random random = new Random();
    private final Props workerProps;

    private Set<ActorRef> activatedWorkers = Collections.emptySet();
    private Iterator<ActorRef> cycle;
    private List<Integer> prevComputations = Collections.singletonList(1);

    @Inject
    public ManagerActor(@AbstractActorProps(actorClass = WorkerActor.class) Props workerProps,
                        @Named("numberOfWorkers") int numberOfWorkers,
                        @Named("expandRate") int expandRate)
    {
        this.workerProps = workerProps;
        this.numberOfWorkers = numberOfWorkers;
        this.expandRate = expandRate;
        receive(ReceiveBuilder
                        .match(ReceiveTimeout.class, r -> scheduleWork())
                        .match(Answer.class, this::process)
                        .match(InProgress.class, this::scheduleNext)
                        .matchAny(this::unhandled)
                        .build()
        );
    }

    @Override
    public void preStart() throws Exception
    {
        activatedWorkers = initializeWorkers(numberOfWorkers, workerProps);
        cycle = Iterators.cycle(activatedWorkers);
        context().setReceiveTimeout(Duration.apply(200, TimeUnit.MILLISECONDS));
    }

    private Set<ActorRef> initializeWorkers(int numberOfWorkers, Props workerProps)
    {
        return Stream.iterate(activatedWorkers.size() + 1, i -> i + 1)
                .limit(numberOfWorkers)
                .map(id -> {
                    final ActorRef ref = context().actorOf(workerProps, "worker-" + id);
                    final int seed = random.nextInt(100);
                    ref.tell(new Config(id, seed), self());
                    return ref;
                })
                .collect(Collectors.toSet());
    }

    private void scheduleWork()
    {
        if (cycle.hasNext())
        {
            final Produce task = new Produce(UUID.randomUUID(), prevComputations, self());
            items.put(task.getId(), task);
            cycle.next().tell(task, self());
        }
    }

    private void scheduleNext(InProgress inProgress)
    {
        if (cycle.hasNext())
        {
            final Produce msg = items.get(inProgress.getId());
            cycle.next().tell(msg, self());
        }
        final Set<ActorRef> newWorkers = initializeWorkers(expandRate, workerProps);
        activatedWorkers.addAll(newWorkers);
        cycle = Iterators.cycle(activatedWorkers);
    }

    private void process(Answer result)
    {
        final Produce input = items.remove(result.getTaskId());
        logger.info("Got computation from {} for {} sum({}) = {}.",
                    result.getWorkerId(),
                    result.getTaskId(),
                    input.getItems(),
                    result.getSum());
        prevComputations = new ArrayList<>(prevComputations);
        prevComputations.add(result.getSum());
    }
}
    
