/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.ffb.demo.akka.streams.consumer;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import com.ffb.demo.akka.streams.consumer.di.ConsumerModule;
import com.fourtyfourblocks.akka.di.BaseModule;
import com.fourtyfourblocks.akka.di.guice.GuiceDiModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.name.Names;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author : pawel
 *         description :
 * @since : 2015-09-18
 */
public class ConsumerApp
{
    private final static Logger logger = LoggerFactory.getLogger(ConsumerApp.class);

    public static void main(String[] args)
    {
        Injector injector = Guice.createInjector(new BaseModule(), new GuiceDiModule(), new ConsumerModule());

        final ActorSystem system = injector.getInstance(ActorSystem.class);
        final ActorRef masterRef1 = injector.getInstance(Key.get(ActorRef.class, Names.named("manager")));

//        final ActorSelection masterSelection = system.actorSelection("/user/manager");
//        final ActorRef masterRef = masterSelection
//                .resolveOne(FiniteDuration.apply(1, TimeUnit.SECONDS))
//                .value()
//                .get()
//                .get();
//
//        if (!masterRef1.equals(masterRef))
//        {
//            system.shutdown();
//        }
    }
}
    
