/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.ffb.demo.akka.streams.consumer.model;

import com.fourtyfourblocks.akka.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

/**
 * @author : pawel
 *         description :
 * @since : 2015-09-17
 */
public class InProgress extends Message
{
    private final static Logger logger = LoggerFactory.getLogger(InProgress.class);

    private final UUID id;

    public InProgress(UUID id)
    {
        this.id = id;
    }

    public UUID getId()
    {
        return id;
    }

    @Override
    protected StringBuilder appendToString(StringBuilder sb)
    {
        return sb.append("id = ").append(id);
    }
}
    
