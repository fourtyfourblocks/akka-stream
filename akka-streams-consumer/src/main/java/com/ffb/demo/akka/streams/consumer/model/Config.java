/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.ffb.demo.akka.streams.consumer.model;

import com.fourtyfourblocks.akka.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author : pawel
 *         description :
 * @since : 2015-09-17
 */
public class Config extends Message
{
    private final static Logger logger = LoggerFactory.getLogger(Config.class);

    private final int id;
    private final int seed;

    public Config(int id, int seed)
    {
        this.id = id;
        this.seed = seed;
    }

    public int getSeed()
    {
        return seed;
    }

    public int getId()
    {
        return id;
    }

    @Override
    protected StringBuilder appendToString(StringBuilder sb)
    {
        return sb.append("id = ").append(id).append(", seed=").append(seed);
    }
}
    
