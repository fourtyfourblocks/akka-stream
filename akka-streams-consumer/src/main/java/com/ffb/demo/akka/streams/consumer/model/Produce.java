/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.ffb.demo.akka.streams.consumer.model;

import akka.actor.ActorRef;
import com.fourtyfourblocks.akka.message.Message;
import com.google.common.collect.ImmutableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.UUID;

/**
 * @author : pawel
 *         description :
 * @since : 2015-09-17
 */
public class Produce extends Message
{
    private final static Logger logger = LoggerFactory.getLogger(Produce.class);

    private final UUID id;
    private final List<Integer> items;
    private final ActorRef ref;

    public Produce(UUID id, List<Integer> items, ActorRef ref)
    {
        this.id = id;
        this.ref = ref;
        this.items = ImmutableList.copyOf(items);
    }

    public List<Integer> getItems()
    {
        return items;
    }

    public UUID getId()
    {
        return id;
    }

    public ActorRef getRef()
    {
        return ref;
    }

    @Override
    protected StringBuilder appendToString(StringBuilder sb)
    {
        return sb.append("id = ").append(id).append(", ref = ").append(ref.path()).append(", items = ").append(items);
    }
}
    
