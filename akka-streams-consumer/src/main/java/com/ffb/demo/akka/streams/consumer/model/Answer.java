/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.ffb.demo.akka.streams.consumer.model;

import com.fourtyfourblocks.akka.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

/**
 * @author : pawel
 *         description :
 * @since : 2015-09-17
 */
public class Answer extends Message
{
    private final static Logger logger = LoggerFactory.getLogger(Answer.class);

    private final int workerId;
    private final UUID taskId;
    private final int sum;

    public Answer(int workerId, UUID taskId, int sum)
    {
        this.workerId = workerId;
        this.taskId = taskId;
        this.sum = sum;
    }

    public int getWorkerId()
    {
        return workerId;
    }

    public UUID getTaskId()
    {
        return taskId;
    }

    public int getSum()
    {
        return sum;
    }

    @Override
    protected StringBuilder appendToString(StringBuilder sb)
    {
        return sb.append("workerId").append(workerId)
                .append(", taskId").append(taskId)
                .append(", result = ").append(sum);
    }
}
    
