/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.ffb.demo.akka.streams.consumer.di;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.stream.ActorMaterializer;
import com.ffb.demo.akka.streams.consumer.actor.ManagerActor;
import com.ffb.demo.akka.streams.consumer.actor.WorkerActor;
import com.fourtyfourblocks.akka.di.AbstractActorProps;
import com.fourtyfourblocks.akka.di.DiAwareCreatorFactory;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.name.Named;

import javax.inject.Singleton;

/**
 * @author : pawel
 *         file name   : AppModule
 *         <p>
 *         description :
 * @since : 2015-07-07
 */
public class ConsumerModule extends AbstractModule
{
    @Override
    protected void configure()
    {
        bind(WorkerActor.class);
    }

    @Provides
    @Singleton
    @Named("manager")
    public ActorRef manager(ActorSystem system, DiAwareCreatorFactory factory)
    {
        final Props props = Props.create(factory.creator(ManagerActor.class));
        return system.actorOf(props, "manager");
    }

    @Provides
    @Singleton
    @Named("numberOfWorkers")
    public int numberOfWorkers()
    {
        return 4;
    }

    @Provides
    @Singleton
    @Named("expandRate")
    public int expandRate()
    {
        return 1;
    }

    @Provides
    @Singleton
    public ActorMaterializer streamMaterializer(ActorSystem producerSystem)
    {
        return ActorMaterializer.create(producerSystem);
    }

    @Provides
    @Singleton
    @AbstractActorProps(actorClass = WorkerActor.class)
    public Props getMasterProps(DiAwareCreatorFactory factory)
    {
        return Props.create(factory.creator(WorkerActor.class));
    }
}
    
