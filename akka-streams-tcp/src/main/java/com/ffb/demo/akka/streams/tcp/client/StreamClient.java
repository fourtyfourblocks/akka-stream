/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.ffb.demo.akka.streams.tcp.client;

import akka.stream.javadsl.Flow;
import akka.stream.javadsl.Tcp;
import akka.util.ByteString;
import scala.concurrent.Future;

/**
 * @author : pawel
 *         description :
 * @since : 2015-08-10
 */
public interface StreamClient
{
    Flow<ByteString, ByteString, Future<Tcp.OutgoingConnection>> connectToStream(String host, int port);
}
