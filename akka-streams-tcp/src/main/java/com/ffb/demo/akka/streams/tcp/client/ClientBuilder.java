/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.ffb.demo.akka.streams.tcp.client;

import akka.stream.javadsl.RunnableGraph;
import scala.runtime.BoxedUnit;

/**
 * @author : pawel
 *         description :
 * @since : 2015-09-05
 */
public interface ClientBuilder
{
    ClientBuilder withConnection(String host, int port);

    RunnableGraph<BoxedUnit> build();
}
