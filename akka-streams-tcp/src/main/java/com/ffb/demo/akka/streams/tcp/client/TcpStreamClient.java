/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.ffb.demo.akka.streams.tcp.client;

import akka.actor.ActorSystem;
import akka.japi.Pair;
import akka.stream.FlowShape;
import akka.stream.UniformFanInShape;
import akka.stream.javadsl.Concat;
import akka.stream.javadsl.Flow;
import akka.stream.javadsl.RunnableGraph;
import akka.stream.javadsl.Source;
import akka.stream.javadsl.Tcp;
import akka.util.ByteString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.concurrent.Future;
import scala.runtime.BoxedUnit;

import javax.inject.Inject;

/**
 * @author : pawel
 *         description :
 * @since : 2015-08-10
 */
public class TcpStreamClient implements StreamClient
{
    private final static Logger logger = LoggerFactory.getLogger(TcpStreamClient.class);
    private final ActorSystem akkaSystem;

    @Inject
    TcpStreamClient(ActorSystem akkaSystem)
    {
        this.akkaSystem = akkaSystem;
    }

    @Override
    public Flow<ByteString, ByteString, Future<Tcp.OutgoingConnection>> connectToStream(String host, int port)
    {
        return Tcp
                .get(akkaSystem)
                .outgoingConnection(host, port);
    }

    public <T> RunnableGraph<Future<Tcp.OutgoingConnection>> createCycle(Source<ByteString, BoxedUnit> initialMessage,
                                                                         Flow<ByteString, ByteString, T> handler,
                                                                         String host, int port)
    {
        final Flow<ByteString, ByteString, Future<Tcp.OutgoingConnection>> connection = connectToStream(host, port);
        final Flow<ByteString, ByteString, BoxedUnit> handShakeFlow = Flow.factory()
                .create(builder -> {
                    final UniformFanInShape<ByteString, ByteString> concat = builder.graph(Concat.create());
                    final FlowShape<ByteString, ByteString> echo = builder.graph(handler);
                    builder
                            .from(initialMessage).to(concat)
                            .from(echo).to(concat);

                    return new Pair<>(echo.inlet(), concat.out());
                });
        return connection.join(handShakeFlow);
    }
}
    
