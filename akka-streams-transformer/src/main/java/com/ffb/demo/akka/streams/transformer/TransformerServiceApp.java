/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 *
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.ffb.demo.akka.streams.transformer;

import akka.stream.ActorMaterializer;
import akka.stream.BidiShape;
import akka.stream.FlowShape;
import akka.stream.javadsl.BidiFlow;
import akka.stream.javadsl.Flow;
import akka.stream.javadsl.Keep;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import akka.stream.javadsl.Tcp;
import akka.util.ByteIterator;
import akka.util.ByteString;
import com.ffb.demo.akka.streams.tcp.client.TcpStreamClient;
import com.ffb.demo.akka.streams.transformer.di.TransformerModule;
import com.ffb.demo.akka.streams.transformer.service.stage.StrGenerator;
import com.fourtyfourblocks.akka.di.BaseModule;
import com.fourtyfourblocks.akka.di.guice.GuiceDiModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;
import scala.runtime.BoxedUnit;

import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * @author : pawel
 *         description :
 * @since : 2015-09-14
 */
public class TransformerServiceApp
{
    private final static Logger logger = LoggerFactory.getLogger(TransformerServiceApp.class);

    public static void main(String[] args) throws Exception
    {
        Injector injector = Guice.createInjector(new BaseModule(), new GuiceDiModule(), new TransformerModule());
        logger.info("Initialized modules.");

        final int port = Integer.parseInt(args[0]);
        logger.info("Starting producer at {}.", port);

        final ByteString delimiter = ByteString.fromInts(Integer.MIN_VALUE, Integer.MAX_VALUE);
        final StrGenerator strGenerator = new StrGenerator(delimiter);

        final ActorMaterializer materializer = injector.getInstance(ActorMaterializer.class);

        final Flow<ByteString, Long, BoxedUnit> decoder = Flow.of(ByteString.class)
                .mapConcat(bs -> {
                    logger.trace("got next number {}.", bs);
                    final ByteIterator iterator = bs.iterator();
                    final ArrayList<Long> result = new ArrayList<>();
                    while (iterator.hasNext())
                    {
                        iterator.next();
                        result.add(iterator.getLong(ByteOrder.BIG_ENDIAN));
                    }
                    return result;
                });

        final Flow<ByteString, ByteString, BoxedUnit> numberRequestResponseFlow = decoder
                .map(n -> {
                    logger.info("result {}", n);
                    return n + "-a";
                })
                .transform(() -> strGenerator);

        final TcpStreamClient tcpStreamClient = injector.getInstance(TcpStreamClient.class);

        final Source<ByteString, BoxedUnit> requestA = Source.single(ByteString.fromString("A").concat(delimiter));
        tcpStreamClient
                .createCycle(requestA, numberRequestResponseFlow, "localhost", port)
                .run(materializer);

        final Source<String, BoxedUnit> requestB = Source.single("B");
        final Sink<Long, Future<Long>> sumSink = Sink.fold(0L, (a, b) -> a + b);

        final Flow<ByteString, ByteString, Future<Tcp.OutgoingConnection>> outgoingFlow = tcpStreamClient
                .connectToStream("localhost", port);

        final Flow<String, ByteString, BoxedUnit> encoder = Flow.of(String.class)
                .map(msg -> ByteString.fromString(msg).concat(delimiter));

        BidiFlow<String, ByteString, ByteString, Long, BoxedUnit> codec =
                BidiFlow.factory().create(builder -> {
                    final FlowShape<String, ByteString> top = builder.graph(encoder);
                    final FlowShape<ByteString, Long> bottom = builder.graph(decoder);
                    return new BidiShape<>(top, bottom);
                });

        final Flow<String, Long, BoxedUnit> stringToNumberRRFlow = codec.join(outgoingFlow);
        final Future<Long> sumOfResponse = requestB
                .via(stringToNumberRRFlow)
                .toMat(sumSink, Keep.right())
                .run(materializer);

        final Long sum = Await.result(sumOfResponse, Duration.apply(30, TimeUnit.SECONDS));
        System.out.println("TransformerServiceApp.main sum = " + sum);
    }
}
    
