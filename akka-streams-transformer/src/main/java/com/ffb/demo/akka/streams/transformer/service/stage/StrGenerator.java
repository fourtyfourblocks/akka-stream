/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.ffb.demo.akka.streams.transformer.service.stage;

import akka.stream.stage.Context;
import akka.stream.stage.PushPullStage;
import akka.stream.stage.SyncDirective;
import akka.util.ByteString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author : pawel
 *         description :
 * @since : 2015-08-10
 */


public class StrGenerator extends PushPullStage<String, ByteString>
{
    private final Logger log = LoggerFactory.getLogger(StrGenerator.class);
    private final ByteString delimiter;

    public StrGenerator(ByteString delimiter)
    {
        this.delimiter = delimiter;
    }

    @Override
    public SyncDirective onPush(String elem, Context<ByteString> ctx)
    {
        log.debug("on push {} - {}.", elem.length(), elem);
        return ctx.push(ByteString.fromString(elem).concat(delimiter));
    }

    @Override
    public SyncDirective onPull(Context<ByteString> ctx)
    {
        log.info("on pull.");
        return ctx.pull();
    }
}
    
