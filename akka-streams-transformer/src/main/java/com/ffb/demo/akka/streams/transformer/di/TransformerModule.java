/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.ffb.demo.akka.streams.transformer.di;

import akka.actor.ActorSystem;
import akka.stream.ActorMaterializer;
import com.ffb.demo.akka.streams.tcp.client.StreamClient;
import com.ffb.demo.akka.streams.tcp.client.TcpStreamClient;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;

import javax.inject.Singleton;

/**
 * @author : pawel
 *         file name   : AppModule
 *         <p>
 *         description :
 * @since : 2015-07-07
 */
public class TransformerModule extends AbstractModule
{
    @Override
    protected void configure()
    {
        bind(StreamClient.class).to(TcpStreamClient.class);
        bind(TcpStreamClient.class);
    }

    @Provides
    @Singleton
    public ActorMaterializer streamMaterializer(ActorSystem system)
    {
        return ActorMaterializer.create(system);
    }
}
    
